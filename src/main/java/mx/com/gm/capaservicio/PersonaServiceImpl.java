package mx.com.gm.capaservicio;

import java.util.List;
import mx.com.gm.capadatos.PersonaDao;
import mx.com.gm.capadatos.domain.Persona;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service("personaService")
@Transactional(propagation=Propagation.SUPPORTS, readOnly=true)
public class PersonaServiceImpl implements PersonaService {

    @Autowired
    private PersonaDao personaDao;

    @Override
    public List<Persona> listarPersonas() {
        return personaDao.findAllPersonas();
    }

    @Override
    public Persona recuperarPersona(Persona persona) {
        return personaDao.findPersonaById(persona.getIdPersona());
    }

    @Transactional(propagation=Propagation.REQUIRED, readOnly=false) 
    @Override
    public void agregarPersona(Persona persona) {
        personaDao.insertPersona(persona);
    }

    @Transactional(propagation=Propagation.REQUIRED, readOnly=false) 
    @Override
    public void modificarPersona(Persona persona) {
        personaDao.updatePersona(persona);
    }

    @Transactional(propagation=Propagation.REQUIRED, readOnly=false) 
    @Override
    public void eliminarPersona(Persona persona) {
        personaDao.deletePersona(persona);
    }
}
